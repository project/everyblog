<?php

/**
 * @file
 * Enables keeping an easily and regularly updated web page or a blog.
 */

/**
 * Implementation of hook_node_info().
 */
function everyblog_node_info() {
	return array(
		'everyblog' => array(
			'name' => t('Blog entry'),
			'module' => 'everyblog',
			'description' => t('A <em>blog entry</em> is a single post to an online journal, or <em>blog</em>, each user can have multiple blogs.'),
		)
	);
}

/**
 * Implementation of hook_perm().
 */
function everyblog_perm() {
	return array(
		'create blog', 
		'create own blog entries', 
		'delete own blog entries', 
		'create any blog entry', 
		'delete any blog entry', 
		'edit own blog entries', 
		'edit any blog entry'
		);
}

/**
 * Implementation of hook_access().
 */
function everyblog_access($op, $node, $account) {
	switch ($op) {
		case 'create':
			// Anonymous users cannot post even if they have the permission. 
			return user_access('create blog entries', $account) && 
				$account->uid ? TRUE : NULL;
		case 'update':
			return user_access('edit any blog entry', $account) || 
				(user_access('edit own blog entries', $account) && 
				($node->uid == $account->uid)) ? TRUE : NULL;
		case 'delete':
			return user_access('delete any blog entry', $account) || 
				(user_access('delete own blog entries', $account) && 
				($node->uid == $account->uid)) ? TRUE : NULL;
	}
}

/**
 * Implementation of hook_user().
 */
function everyblog_user($type, &$edit, &$user) {
	if ($type == 'view' && user_access('create blog entries', $user)) {
		$user->content['summary']['blog'] =	array(
			'#type' => 'user_profile_item',
			'#title' => t('Blog'),
			'#value' => l(t('View recent blog entries'), "blog/$user->uid", array('attributes' => array('title' => t("Read @username's latest blog entries.", array('@username' => $user->name))))),
			'#attributes' => array('class' => 'blog'),
		);
	}
}

/**
 * Implementation of hook_help().
 */
/*
function blog_help($path, $arg) {
}
*/

/**
 * Implementation of hook_form().
 */
function everyblog_form(&$node) {
	global $nid, $user;
	
	$form['form_id']['#value'] = 'everyblog_form';

	$blog_id=(isset($_GET['blog']) ? (int) $_GET['blog'] : $node->blog_id);

	//
	// We need some JavaScript to handle hiding and unhiding the textbox
	// where the user can enter the name for a new blog, also, we want 
	// hint text to be clickcleared and clickrecalled
	drupal_add_js("
		function handleBlogNameVisibility(e) {
			var namebox = document.getElementById('my-blog-name-wrapper');
			var categorybox = document.getElementById('my-blog-category-wrapper');
			if( e.selectedIndex == 1 ) {
				namebox.style.display = '';
				categorybox.style.display = '';
			}
			else {
				namebox.style.display = 'none';
				categorybox.style.display = 'none';			
			}
		}
		
		function clickclear(thisfield, defaulttext) {
			if (thisfield.value == defaulttext) {
				thisfield.value = '';
			}
		}

		function clickrecall(thisfield, defaulttext) {
			if (thisfield.value == '') {
				thisfield.value = defaulttext;
			}
		}
	",
		 "inline"
	);
	
	$iid = isset($_GET['iid']) ? (int)$_GET['iid'] : 0;
	$type = node_get_types('type', $node);

	$uid = $user->uid;

	// Can the user create entries to any blog?
	if (user_access('create any blog entries', $user)) {
		// Since the user can add to any blog we need to list ALL the blogs
		$query = "SELECT blog_id, blog_name FROM {everyblogs}";
	}
	else {
		// Get a list of blogs that the current user has created
		$query = "SELECT blog_id, blog_name FROM {everyblogs} WHERE uid = $uid";
	}

	// Run the query to get the proper list of blogs
	$result = db_query($query);
	$blogs = array(
		'0' => t('Select a blog'),
		'-1' => t('Create a new blog'),
		);
	while ($blog = db_fetch_object($result)) {
		$blogs[$blog->blog_id] = $blog->blog_name;
	}

	// Store the user id in the form for quick recall later
	$form['uid'] = array(
		'#type' => 'value',
		'#value' => $uid);
	
	// This select box allows the user to select which blog they are creating an
	// entry for, or to create a new blog
	$form['blog_id'] = array(
		'#type' => 'select',
		'#title' => t('Blog'),
		'#default_value' => $blog_id,
		'#weight' => -5,
		'#required' => TRUE,
		'#options' => $blogs,
		'#attributes' => array(
			'onchange' => "handleBlogNameVisibility(this)",
			),
		);
	 
	 // If the user has choosen to create a new blog this text box, which is hidden
	 // using the #attributes by setting the style.display to 'none', allows the user
	 // to name their new blog
	$form['blog_name'] = array(
		 '#type' => 'textfield',
		 '#title' => t('Name'),
		 '#description' => t('The name for your new blog'),
		 '#prefix' => "<span id='my-blog-name-wrapper' style='display:none;'>",
		 '#suffix' => ' </span>',
		 '#weight' => -4,		 
		);
	
	$vid = variable_get('everyblog_taxonomy_category', -1);
	if ($vid > -1) {
		$raw_categories = taxonomy_get_tree($vid);
		foreach($raw_categories as $category) {
			$categories[$category->tid] = $category->name;
		}
		$form['blog_category'] = array(
			 '#type' => 'select',
			 '#default_value' => 0,
			 '#options' => $categories,
			 '#title' => t('Category'),
			 '#description' => t('Select the category for your new blog. ') .
			 	(user_access('administer taxonomy') ? t('<i>Adding new categories can be done by adding terms to the correct vocabulary</i>') : ''),
			 '#prefix' => "<span id='my-blog-category-wrapper' style='display:none;'>",
			 '#suffix' => ' </span>',
			 '#weight' => -3, 
			);
	}
	else {
		$form['blog_category'] = array(
			 '#type'=> 'markup',
			 '#title' => t('Category'),
			 '#value' => t('You need to pick a vocubulary to use as the categories for the blogs, this can be done in settings page for EveryBlog'),
			 '#prefix' => "<span id='my-blog-category-wrapper' style='display:none;'>",
			 '#suffix' => ' </span>',
			 '#weight' => -3, 
		);
	}
		
	if (empty($node->body)) {
		// If the user clicked a "blog it" link, we load the data from the
		// database and quote it in the blog.
		if ($nid && $blog = node_load($nid)) {
			$node->body = '<em>'. $blog->body .'</em> ['. l($blog->blog_name, "node/$nid") .']';
		}

		if ($iid && $item = db_fetch_object(db_query(
			'SELECT 
				i.*, f.title as ftitle, f.link as flink 
			 FROM 
			 	{aggregator_item} i, {aggregator_feed} f 
			 WHERE 
			 	i.iid = %d AND 
			 	i.fid = f.fid', $iid))) {
			$node->title = $item->title;
			// Note: $item->description has been validated on aggregation.
			$node->body = '<a href="'. check_url($item->link) .'">'. check_plain($item->title) .'</a> - <em>'. $item->description .'</em> [<a href="'. check_url($item->flink) .'">'. check_plain($item->ftitle) ."</a>]\n";
		}

	}

	// The title for the blog entry
	$form['title'] = array(
		'#type' => 'textfield', 
		'#title' => check_plain($type->title_label), 
		'#required' => TRUE, 
		'#default_value' => !empty($node->title) ? $node->title : NULL, 
		'#weight' => -1);
		
	$form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
	return $form;
}

/**
 * Implementatoin of hook_insert()
 */
function everyblog_insert($node) {
	// A blog_id of -1 means the user choose to create a new blog
	if ($node->blog_id == -1) {
		// insert a everyblog record into the table to keep track of the
		// new blog
		db_query("INSERT INTO {everyblogs} SET blog_name='%s', uid='%s', tid='%s'", 
			$node->blog_name, 
			$node->uid,
			$node->blog_category);
		$blog_id = db_last_insert_id('everyblogs', 'blog_id');	
	}
	else {
		$blog_id = $node->blog_id;
	}
	
	// associate the newely created node with the blog it belongs to
	db_query("INSERT INTO {everyblogs_to_nid} SET blog_id='%s', nid='%s'", 
		$blog_id, 
		$node->nid);
}

/**
 * Implementation of hook_update() 
 */
function everyblog_update($node) {
	// A blog_id of -1 means the user choose to create a new blog
        if ($node->blog_id == -1) {
		// insert a everyblog record into the table to keep track of the
		// new blog
		db_query("INSERT INTO {everyblogs} SET blog_name='%s', uid='%s', tid='%s'",
			$node->blog_name,
			$node->uid,
		$node->blog_category);
		$blog_id = db_last_insert_id('everyblogs', 'blog_id');
	}
	else {
		$blog_id = $node->blog_id;
	}

	// update the blog matching
	db_query("UPDATE {everyblogs_to_nid} SET blog_id='%s' WHERE nid='%s'",
		$blog_id,
		$node->nid);
}

/**
 * Implementation of hook_view().
 */
function everyblog_view($node, $teaser = FALSE, $page = FALSE) {
	if ($page) {
		// Breadcrumb navigation
		drupal_set_breadcrumb(array(
			l(t('Home'), NULL), 
			l(t('Blogs'), 'blogs'), 
			l($node->blog_name, 'blog/'. $node->blog_id)
			));
		
	}
	return node_prepare($node, $teaser);
}

/**
 * Implementation of hook_load().
 */
function everyblog_load($node) {
	$additions = db_fetch_object(db_query('SELECT b.blog_id, b.blog_name FROM {everyblogs} AS b, {everyblogs_to_nid} AS b_to_n WHERE b_to_n.nid = %s AND b_to_n.blog_id = b.blog_id', $node->nid));
	
	return $additions;
}

/**
 * Implementation of hook_link().
 */
function everyblog_link($type, $node = NULL, $teaser = FALSE) {
	$links = array();

	if ($type == 'node' && $node->type == 'blog') {
		if (arg(0) != 'blog' || arg(1) != $node->uid) {
			$links['blog_usernames_blog'] = array(
				'title' => t("@username's blog", array('@username' => $node->name)),
				'href' => "blog/$node->uid",
				'attributes' => array('title' => t("Read @username's latest blog entries.", array('@username' => $node->name)))
			);
		}
	}

	return $links;
}

/**
 * Implementation of hook_menu().
 */
function everyblog_menu() {
	global $user;
	
	// Show a list of all blogs
	$items['blogs'] = array(
		'title' => 'Blogs',
		'page callback' => 'everyblog_list_blogs',
		'access arguments' => array('access content'),
		'type' => MENU_SUGGESTED_ITEM,
		'file' => 'everyblog.pages.inc',
	);

	$items['add_entry'] = array(
		'title' => 'Add new entry',
		'page callback' => 'everyblog_form',
		'access arguments' => array('create own blog entry'),
		'type' => MENU_LOCAL_TASK,
		'file' => 'everyblog.module',
	);
	
	// Get a list of blogs that are by a certian user
	$items['blogs/user/%'] = array(
		'title' => 'Blogs By User',
		'page callback' => 'everyblog_list_blogs_by_user',
		'page arguments' => array(2),
		'access arguments' => array('access content'),
		'type' => MENU_CALLBACK,
		'file' => 'everyblog.pages.inc',
	);
	
	// Get a list of blogs that are in a specific category
	$items['blogs/category/%'] = array(
		'title' => 'Blogs By Category',
		'page callback' => 'everyblog_list_blogs_by_category',
		'page arguments' => array(2),
		'access arguments' => array('access content'),
		'type' => MENU_CALLBACK,
		'file' => 'everyblog.pages.inc',
	);
	
	// Show a single blog
	$blogs = array();
	$items['blog/%'] = array(
		'page callback' => 'everyblog_page',
		'page arguments' => array(1),
		'access callback' => 'everyblog_page_user_access',
		'access arguments' => array(1),
		'type' => MENU_CALLBACK,
		'file' => 'everyblog.pages.inc',
	);
	
	// Show the settings for EveryBlog
	$items['admin/settings/everyblog'] = array(
		'title' => 'EveryBlog module settings',
		'description' => 'Allows configuration of the EveryBlog module.',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('everyblog_admin'),
		'access arguments' => array('access administration pages'),
		'type' => MENU_NORMAL_ITEM,
	 );
	
	
	$items['blogs/user/%/feed'] = array(
		'title' => 'Blogs By User',
		'page callback' => 'everyblog_feed_user',
		'page arguments' => array(2),		
		'access arguments' => array('access content'),
		'type' => MENU_CALLBACK,
		'file' => 'everyblog.pages.inc',
	);
	
	$items['blogs/category/%/feed'] = array(
		'title' => 'Blogs By Category',
		'page callback' => 'everyblog_feed_category',
		'page arguments' => array(2),		
		'access arguments' => array('access content'),
		'type' => MENU_CALLBACK,
		'file' => 'everyblog.pages.inc',
	);
	
	$items['blog/%/feed'] = array(
		'title' => 'Blog',
		'page callback' => 'everyblog_feed_blog',
		'page arguments' => array(1),		
		'access arguments' => array('access content'),
		'type' => MENU_CALLBACK,
		'file' => 'everyblog.pages.inc',
	);
	
	$items['blogs/feed'] = array(
		'title' => t('Blogs'),
		'page callback' => 'everyblog_feed_all',
		'access arguments' => array('access content'),
		'type' => MENU_CALLBACK,
		'file' => 'everyblog.pages.inc',
	);
	

	return $items;
}

/**
 * Access callback for user blog pages.
 */
function everyblog_page_user_access($account) {
	// The visitor must be able to access the site's content.
	// For a blog to 'exist' the user must either be able to
	// create new blog entries, or it must have existing posts.
	return user_access('access content');
	//return $account->uid && user_access('access content') && (user_access('create blog entries', $account) || _everyblog_post_exists($account));
}

/**
 * Helper function to determine if a user has blog posts already.
 */
function _everyblog_post_exists($account) {
	return (bool)db_result(db_query_range(db_rewrite_sql("SELECT 1 FROM {node} n WHERE n.type = 'blog' AND n.uid = %d AND n.status = 1"), $account->uid, 0, 1));
}

/**
 * Implementation of hook_block().
 *
 * Displays the most recent 10 blog titles.
 */
function everyblog_block($op = 'list', $delta = 0) {
	global $user;
	if ($op == 'list') {
		$block[0]['info'] = t('Recent blog posts');
		return $block;
	}
	else if ($op == 'view') {
		if (user_access('access content')) {
			$result = db_query_range(db_rewrite_sql("SELECT n.nid, n.title, n.created FROM {node} n WHERE n.type = 'everyblog' AND n.status = 1 ORDER BY n.created DESC"), 0, 10);
			if ($node_title_list = node_title_list($result)) {
				$block['content'] = $node_title_list;
				$block['content'] .= theme('more_link', url('blog'), t('Read the latest blog entries.'));
				$block['subject'] = t('Recent blog posts');
				return $block;
			}
		}
	}
}

/**
 * Implementation of hook_admin()
 */
function everyblog_admin() {

	$options[-1] = '-None-';
	foreach(taxonomy_get_vocabularies() as $vocab) {
		$options[$vocab->vid] = $vocab->name;
	}
	$form['everyblog_taxonomy_category'] = array(
		'#type' => 'select',
		'#title' => t('Blog Taxonomy Catogory'),
		'#description' => t('The vocabulary to use for categorizing blogs.'),
		'#default_value' => variable_get('everyblog_taxonomy_category', 'Blog Catagories'),
		'#options' => $options
	);
	
	return system_settings_form($form);
}