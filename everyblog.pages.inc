<?php

/**
 * @file
 * Page callback file for the blog module.
 */

/**
 * Handles displaying a list of blogs in a couple of ways, either a striaght listing
 * of all blogs or a list of the blogs grouped by the user that created them
 */
function everyblog_list_blogs() {
	$output ='';

	// The path might be "blogs/user" in which case we show a list of
	// blogs grouped by user
	if (func_num_args() == 1 && strtolower(func_get_arg(0)) == 'user') {
		drupal_set_title($title=t('All Blogs Grouped by User'));
	
		// Setup a basic breadcrumb
		drupal_set_breadcrumb(array(
			l(t('Home'), NULL), 
			l(t('Blogs'), 'blogs')
			));
		 
		// Get a list of users who have made blogs
		$users_result = db_query('SELECT uid FROM {everyblogs} GROUP BY uid');
		
		// For each user who has blogs
		while($u = db_fetch_object($users_result)) {
			// load the user so we can get their username
			$user = user_load($u->uid);
			$output .= l($user->name, "blogs/user/{$u->uid}");
			
			// get a list of blogs that the user has
			$blogs = array();
			$query = 'SELECT blog_id, blog_name FROM {everyblogs} WHERE uid=%s ORDER BY blog_name';
			$result = db_query($query, $user->uid);
			while($blog = db_fetch_object($result)) {
				$blogs[] = l($blog->blog_name, 'blog/'._everyblog_encode($blog->blog_name));
			}
			if (count($blogs) > 0) {
				$output .= theme('item_list', $blogs);
			}
			else {
				drupal_set_message(t('There are no blogs by this user yet.'), 'status');
			}
		}
	}
	else if (func_num_args() == 1 && strtolower(func_get_arg(0)) == 'category') {
		drupal_set_title($title=t('All Blogs Grouped by Category'));
		
		// Setup a basic breadcrumb
		drupal_set_breadcrumb(array(
			l(t('Home'), NULL), 
			l(t('Blogs'), 'blogs')
			));
		 
		// Get a list of categories blogs are in
		$category_list = db_query('SELECT tid FROM {everyblogs} GROUP BY tid');
		
		// For each user who has blogs
		while($category = db_fetch_object($category_list)) {
			// load the user so we can get their username
			$category = taxonomy_get_term($category->tid);
			$output .= l($category->name, "blogs/category/{$category->tid}");
			
			// get a list of blogs under this category
			$blogs = array();
			$query = 'SELECT blog_id, blog_name FROM {everyblogs} WHERE tid=%s ORDER BY blog_name';
			$result = db_query($query, $category->tid);
			while($blog = db_fetch_object($result)) {
				$blogs[] = l($blog->blog_name, 'blog/'._everyblog_encode($blog->blog_name));
			}
			if (count($blogs) > 0) {
				$output .= theme('item_list', $blogs);
			}
			else {
				drupal_set_message(t('There are no blogs in this category yet.'), 'status');
			}
				
		}
	}
	// Show a regular listing of all blogs
	else {	
		drupal_set_title($title=t('All Blogs'));
	
		// grab a list of all blogs
		$query = 'SELECT blog_id, blog_name FROM {everyblogs} ORDER BY blog_name';
		$result = db_query($query);
		while($blog = db_fetch_object($result)) {
			$blogs[] = l($blog->blog_name, 'blog/'._everyblog_encode($blog->blog_name));
		}
		$output .= theme('item_list', $blogs);
		
		// give the user an option to see the blogs grouped by user
		$output .= '<br>';
		$output .= l(t('View Blogs Grouped by User'), 'blogs/user');
		$output .= '<br>';
		$output .= l(t('View Blogs Grouped by Category'), 'blogs/category');
	}
	
	drupal_add_feed(url('blogs/feed'), t('RSS - All Blogs'));
	
	return $output;
}

/**
 * Lists all the blogs by a particular user
 *
 * @param $uid mixed if its a number its assumed to be a uid otherwise its taken to be the username
 */
function everyblog_list_blogs_by_user($uid) {
	$output ='';
	
	if (is_numeric($uid)) {
		$user = user_load($uid);
		if ($user == FALSE) {
			drupal_set_message(
				t('No matching user found. Please check for typing errors in the URL.'),
				'error');
			return '';
		}
	}
	else {
		$user = user_load(array('name'=>$uid));
		if ($user == FALSE) {
			drupal_set_message(
					t("No matching user found for username '@uid' Please check for typing errors in the URL.",
					array('@uid'=>$uid)
					),
				'error');
			return '';
		}
	}
	$uid = $user->uid;
	drupal_set_title($title=t('Blogs by ') . $user->name);
	
	$query = 'SELECT blog_id, blog_name FROM {everyblogs} WHERE uid='.$uid.' ORDER BY blog_name';
	$result = db_query($query);
	while($blog = db_fetch_object($result)) {
		$blogs[] = l($blog->blog_name, 'blog/'._everyblog_encode($blog->blog_name));
	}
	$output .= theme('item_list', $blogs);
	
	drupal_add_feed(url('blogs/user/'. $user->name .'/feed'), t('RSS - Blogs by !username', array('!username' => $user->name)));
	return $output;	
}

/**
 * Lists all the blogs by a particular user
 *
 * @param $uid mixed if its a number its assumed to be a uid otherwise its taken to be the username
 */
function everyblog_list_blogs_by_category($tid) {
	$output ='';
	
	// If the URL part is numeric then its a taxonomy term id
	if (is_numeric($tid)) {		
		$category = taxonomy_get_term($tid);
		if ($category == FALSE) {
			drupal_set_message(t('No matching category was found. Please check for typing errors in the URL.'), 'error');
			return '';
		}
	}
	// The URL part is a category name, so look it up
	else {
		$category = taxonomy_get_term_by_name($tid);
		if ($category == FALSE) {
			drupal_set_message(
					t("No matching category was found for '@tid'. Please check for typing errors in the URL.",
					array('@tid'=>$tid)
					), 
				'error');
			return '';
		}
		$category = $category[0];
	}
	$tid = $category->tid;
	drupal_set_title($title=t('Blogs in ') . $category->name);
	
	$query = 'SELECT blog_id, blog_name FROM {everyblogs} WHERE tid='.$tid.' ORDER BY blog_name';
	$result = db_query($query);
	while($blog = db_fetch_object($result)) {
		$blogs[] = l($blog->blog_name, 'blog/'._everyblog_encode($blog->blog_name));
	}
	$output .= theme('item_list', $blogs);
	
	drupal_add_feed(url('blogs/category/'. $category->name .'/feed'), t('RSS - Blogs in !category', array('!category' => $category->name)));
	return $output;
}

/**
 * This encodes strings in such a way that they are both human readable and URL safe.
 * Spaces are replaced by underscores '_' then the string is urlencoded.
 *
 * @param $str string the string to encode in a safe way
 */
function _everyblog_encode($str) {
	return urlencode(str_replace(' ', '_', $str));
}

/**
 * Displays recent blog entries of a given blog.
 *
 * @param $blog_id mixed if its a number then its taken to be the blogs id, otherwise its
 *		taken to be the blogs name, either URL encoded or with spaces replaced by
 * 		underscores '_'
 */
function everyblog_page($blog_id) {
	global $user;
	
	// Load the blog id, since $blog_id can be an actual blog id or the blog name
	if ( !is_numeric($blog_id) ) {
		$blog_name = urldecode($blog_id);
		$blog_name = str_replace('_', ' ', $blog_name);
		$result = db_query("SELECT blog_id FROM {everyblogs} WHERE blog_name LIKE \"%s\"", $blog_name);
		$blog_id = db_fetch_object($result);
		$blog_id = $blog_id->blog_id;
	}
	
	// Get the blog title
	$query = 'SELECT * FROM {everyblogs} WHERE blog_id=%s';
	$blog = db_fetch_object(db_query($query, $blog_id));

	drupal_set_title($title = $blog->blog_name);
	drupal_set_breadcrumb(array(
		l(t('Home'), NULL),
		l(t('Blogs'), 'blogs'),
		$blog->blog_name,
		));


	$items = array();

	if (($blog->uid == $user->uid) && user_access('create own blog entries')) {
		$items[] = l(t('Post new blog entry.'), "node/add/everyblog" , 
			array('query'=>array('blog'=>$blog->blog_id)));
	}
	else if ($account->uid == $user->uid) {
		$items[] = t('You are not allowed to post a new blog entry.');
	}

	$output .= theme('item_list', $items);

	$query = <<<EOF
SELECT n.nid, n.sticky, n.created 
	FROM 
		{node} AS n,
		{everyblogs_to_nid} AS e_to_n
	WHERE
		n.type = 'everyblog' AND
		n.nid = e_to_n.nid AND
		e_to_n.blog_id = '$blog_id'
	ORDER BY n.sticky DESC, n.created DESC
EOF;

	$result = pager_query($query, 
		variable_get('default_nodes_main', 10), 0, NULL, $account->uid);
	$has_posts = FALSE;
	
	while ($node = db_fetch_object($result)) {
		$output .= node_view(node_load($node->nid), 1);
		$has_posts = TRUE;
	}
	
	if ($has_posts) {
		$output .= theme('pager', NULL, variable_get('default_nodes_main', 10));
	}
	else {
		if ($account->uid == $user->uid) {
			drupal_set_message(t('You have not created any blog entries.'));
		}
		else {
			drupal_set_message(t('No blog entries have been created in this blog.'));
		}
	}
	drupal_add_feed(url('blog/'. $blog->blog_name .'/feed'), t('RSS - !title', array('!title' => $title)));

	return $output;
}

/**
 * Menu callback; displays a Drupal page containing recent blog entries of all users.
 */
function everyblog_page_last() {
	global $user;

	$output = '';
	$items = array();

	if (user_access('edit own blog')) {
		$items[] = l(t('Create new blog entry.'), "node/add/blog");
	}

	$output = theme('item_list', $items);

	$result = pager_query(db_rewrite_sql("SELECT n.nid, n.created FROM {node} n WHERE n.type = 'blog' AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC"), variable_get('default_nodes_main', 10));
	$has_posts = FALSE;

	while ($node = db_fetch_object($result)) {
		$output .= node_view(node_load($node->nid), 1);
		$has_posts = TRUE;
	}
	
	if ($has_posts) {
		$output .= theme('pager', NULL, variable_get('default_nodes_main', 10));
	}
	else {
		drupal_set_message(t('No blog entries have been created.'));
	}
	drupal_add_feed(url('blog/feed'), t('RSS - blogs'));

	return $output;
}

/**
 * Menu callback; displays an RSS feed containing recent blog entries of a given user.
 */
function everyblog_feed_user($uid) {
	// load the user
	if (is_numeric($uid)) {
		$user = user_load($uid);
	}
	else {
		$user = user_load(array('name'=>$uid));
	}
	if ($user == FALSE) {
		$channel['title'] = t('No blogs by @username', array('@username' => $uid));
		$channel['link'] = url("/", array('absolute' => TRUE));
		$items = array();
		node_feed($items, $channel);
		return;
	}
	$uid = $user->uid;

	// Set the channel information
	$channel['title'] = t('Blogs by @username', array('@username' => $user->name));
	$channel['link'] = url("blogs/user/$uid", array('absolute' => TRUE));
	
	$query = <<<EOF
SELECT e.blog_id, e.blog_name, n.nid, n.created 
FROM {everyblogs} AS e, {everyblogs_to_nid} AS etn, {node} AS n 
WHERE e.uid=$uid AND e.blog_id = etn.blog_id AND etn.nid = n.nid ORDER BY n.created DESC
EOF;
	
	// Run the query 
	$result = db_query_range($query, 0, variable_get('feed_default_items', 10));

	// get the feed items
	while($entry = db_fetch_object($result)) {
		$entries[] = $entry->nid;
	}
	node_feed($entries, $channel);
}

/**
 * Menu callback; displays an RSS feed containing recent blog entries in a given category
 */
function everyblog_feed_category($tid) {
	// If the URL part is numeric then its a taxonomy term id
	if (is_numeric($tid)) {		
		$category = taxonomy_get_term($tid);
	}
	// The URL part is a category name, so look it up
	else {
		$category = taxonomy_get_term_by_name($tid);
		$category = $category[0];
	}
	if ($category === FALSE) {
		$channel['title'] = t('No category @tid', array('@tid'=>$tid));
		$channel['link'] = url("/", array('absolute' => TRUE));
		$items = array();
		node_feed($items, $channel);
		return;
	}
	$tid = $category->tid;

	// Set the channel information
	$channel['title'] = t('Blogs in @category', array('@category' => $category->name));
	$channel['link'] = url("blogs/category/$tid", array('absolute' => TRUE));
	
	$query = <<<EOF
SELECT e.blog_id, e.blog_name, n.nid, n.created 
FROM {everyblogs} AS e, {everyblogs_to_nid} AS etn, {node} AS n 
WHERE e.tid=$tid AND e.blog_id = etn.blog_id AND etn.nid = n.nid ORDER BY n.created DESC
EOF;
	
	// Run the query 
	$result = db_query_range($query, 0, variable_get('feed_default_items', 10));

	// get the feed items
	while($entry = db_fetch_object($result)) {
		$entries[] = $entry->nid;
	}
	node_feed($entries, $channel);
}

/**
 * Menu callback; displays an RSS feed containing recent blog entries in a given blog.
 */
function everyblog_feed_blog($blog_id) {
	// Load the blog id, since $blog_id can be an actual blog id or the blog name
	if ( !is_numeric($blog_id) ) {
		$blog_name = urldecode($blog_id);
		$blog_name = str_replace('_', ' ', $blog_name);
		$result = db_query("SELECT blog_id FROM {everyblogs} WHERE blog_name LIKE \"%s\"", $blog_name);
		$blog_id = db_fetch_object($result);
		$blog_id = $blog_id->blog_id;
	}
	// Get the blog title
	$query = 'SELECT * FROM {everyblogs} WHERE blog_id=%s';
	$blog = db_fetch_object(db_query($query, $blog_id));

	// Set the channel information
	$channel['title'] = t('@blogname: Recent Entries', array('@blogname' => $blog->blog_name));
	$channel['link'] = url("blog/{$blog->blog_name}", array('absolute' => TRUE));
	
	$query = <<<EOF
SELECT e.blog_id, e.blog_name, n.nid, n.created 
FROM {everyblogs} AS e, {everyblogs_to_nid} AS etn, {node} AS n 
WHERE e.blog_id=$blog_id AND e.blog_id = etn.blog_id AND etn.nid = n.nid ORDER BY n.created DESC
EOF;
	
	// Run the query 
	$result = db_query_range($query, 0, variable_get('feed_default_items', 10));

	// get the feed items
	while($entry = db_fetch_object($result)) {
		$entries[] = $entry->nid;
	}
	node_feed($entries, $channel);
}

/**
 * Menu callback; displays an RSS feed containing recent blog entries of all blogs.
 */
function everyblog_feed_all() {
	$result = db_query_range(db_rewrite_sql("SELECT n.nid, n.created FROM {node} n WHERE n.type = 'blog' AND n.status = 1 ORDER BY n.created DESC"), 0, variable_get('feed_default_items', 10));
	$channel['title'] = variable_get('site_name', 'Drupal') .' blogs';
	$channel['link'] = url('blog', array('absolute' => TRUE));

	$items = array();
	while ($row = db_fetch_object($result)) {
		$items[] = $row->nid;
	}

	node_feed($items, $channel);
}