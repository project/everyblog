Description:
EveryBlog is the every man of blog modules. Its a replacement for Drupal's built-in Blog module that 
allows users to have multiple blogs. Each blog has its own name instead of being "So-in-so's Blog" and 
blogs can be grouped into categories. Categories are actually just a vocabulary that you have chosen to 
use for categorizing blogs (set via the settings page).

When you go to "Create Content"->"Blog" you are asked for which blog you'd like to post to. (Users can 
only post to blogs they've created.) Or, instead of selecting an existing blog, you can choose to create 
a new blog.

When creating a new blog you will give it a name and select which category the blog belongs to. Since 
categories are really vocabularies, adding a new category must be done through the Taxonomy module.

To go straight to a blog you can use:
"www.yoursite.com/blog/Your_Blog_Name"

There are also several ways to get a list of blogs on a site:
1) Going to "www.yoursite.com/blogs" shows a list of all the blogs on the site alphabetically.
2) Going to "www.yoursite.com/blogs/user" shows a list of the blogs grouped by the user that created 
them.
3) Going to "www.yoursite.com/blogs/category/" shows a list of blogs grouped by category.
4) Going to "www.yoursite.com/blogs/user/johndoe" shows a list of all the blogs by the user 'johndoe'
5) Going to "www.yoursite.com/blogs/category/projects" shows a list of all the blogs in the category 
'projects'.

Upcoming Features:
    * Blogtheme integration


Author:
Nathaniel Troutman (http://drupal.org/user/103652)
EveryBlog (http://drupal.org/project/everyblog)
